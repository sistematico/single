<?php

// Seta o $id com os dados de $_GET['id'] se houverem.
$id = (isset($_GET['id'])) ? $_GET['id'] : false;

// Cria o arquivo.
$arquivo = "banco.db";
$db = null;

// Cabeçahos de mensagens.
$info = '<div class="notification is-info"><button class="delete"></button>';
$warning = '<div class="notification is-warning"><button class="delete"></button>';
$danger = '<div class="notification is-danger"><button class="delete"></button>';
$mensagem = '';

// Seta as condicionais.
$insere = isset($_POST['nomeInsere']) && isset($_POST['emailInsere']);

$edita = isset($_POST['idEdita']) && !empty($_POST['idEdita']);

$editar = isset($_POST['idEditar'])
    && isset($_POST['nomeEditar'])
    && isset($_POST['emailEditar'])
    && !empty($_POST['idEditar'])
    && !empty($_POST['nomeEditar'])
    && !empty($_POST['emailEditar']);

$apaga = isset($_POST['idApaga']) && !empty($_POST['idApaga']);

// Conecta ao banco de dados.
try {
    $db = new PDO('sqlite:' . $arquivo);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
} catch (\PDOException $e) {
    // exit('A conexão com o banco de dados não pode ser feita: ' . $e->getMessage());
    $mensagem = $danger;
    $mensagem .= "A conexão com o banco de dados não pode ser feita: " . $e->getMessage();
    $mensagem .= '</div>';
}

// Cria a tabela.
try {
    $db->exec("CREATE TABLE IF NOT EXISTS students (id INTEGER PRIMARY KEY, nome STRING, email STRING);");
} catch (\PDOException $e) {
    //exit('Erro na criação da tabela: ' . $e->getMessage());
    $mensagem = $danger;
    $mensagem .= "Erro na criação da tabela: " . $e->getMessage();
    $mensagem .= '</div>';
}

// Insere
if ($insere) {
    $nome = trim($_POST['nomeInsere']);
    $email = trim($_POST['emailInsere']);

    $sql = "INSERT INTO students (nome, email) VALUES ('$nome', '$email')";

    if ($db->exec($sql)) {
        $mensagem = $info;
        $mensagem .= "Dados inseridos com sucesso.";
        $mensagem .= '</div>';
    } else {
        $mensagem = $danger;
        $mensagem .= "Falha na inserção de dados.";
        $mensagem .= '</div>';
    }
} else if ($edita) { // Editar
    $id = $_POST['idEdita'];
    $consulta = "SELECT * FROM students WHERE id=$id";
    $resultado = $db->query($consulta);
    $data = $resultado->fetch(); // set the row in $data
} else if ($editar) { // Edita
    $id = $_POST['idEditar'];
    $nome = $_POST['nomeEditar'];
    $email = $_POST['emailEditar'];

    $query = "UPDATE students set nome='$nome', email='$email' WHERE id=$id";

    if ($db->exec($query)) {
        $mensagem = $info;
        $mensagem .= "Registro editado com sucesso.";
        $mensagem .= '</div>';
    } else {
        $mensagem = $danger;
        $mensagem .= "Erro ao editar registro.";
        $mensagem .= '</div>';
    }
} else if ($apaga) { // Apaga
    $id = $_POST['idApaga'];
    $consulta = "DELETE FROM students WHERE id=$id";
    if ($db->query($consulta)) {
        $mensagem = $info;
        $mensagem .= "Registro apagado com sucesso.";
        $mensagem .= '</div>';
    } else {
        $mensagem = $danger;
        $mensagem .= "Erro ao apagar registro.";
        $mensagem .= '</div>';
    }
}

// Lista
$sql = "SELECT * FROM students";
$query = $db->query($sql);
$listagem = $query->fetchAll(); // set the row in $data

$db = null;

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Exemplo de SPA(Single Page Application)">
        <meta name="author" content="Lucas Saliés Brum">
        <title>Single Page CRUD</title>
        <meta property="og:title" content="SPA CRUD" />
        <meta property="og:description" content="Exemplo de SPA(Single Page Application)" />
        <meta property="og:type" content="website" />
        <meta property="og:url" content="https://spa.lucasbrum.net" />
        <meta property="og:image" content="img/superman.png" />
        <link rel="stylesheet" href="css/bulma.min.css?v=<?php echo time(); ?>">
        <link rel="stylesheet" href="css/fontawesome-5.11.2.min.css?v=<?php echo time(); ?>">
        <style>
            html { background-image: linear-gradient(-90deg, #a8ff78, #78ffd6); }
        </style>
        <link rel="shortcut icon" href="img/favicon.ico">
    </head>
<body>
    <section class="section">
        <div class="container">
            <h1 class="title">Single Page CRUD</h1>
            <p class="subtitle">Exemplo de aplicação de uma única página feito com o <strong>Bulma</strong> e PHP</p>
            <?php if (isset($mensagem) && !empty($mensagem)) { echo $mensagem; } ?>
            <div class="tile is-ancestor">
                <div class="tile is-vertical is-parent">

                    <div class="tile is-child notification is-primary">
                        <!-- INSERE -->
                        <h2 class="title">Inserir</h2>
                        <p class="subtitle">Inserir registros.</p>
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label">Nome</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" name="nomeInsere" type="text" placeholder="Seu nome">
                                        </p>
                                    </div>
                                </div>
                                <div class="field-label is-normal">
                                    <label class="label">E-mail</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" name="emailInsere" type="text" placeholder="Seu e-mail">
                                        </p>
                                    </div>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="button" name="inserir" type="submit" value="Enviar">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tile is-child notification is-warning">
                        <!-- ATUALIZA -->
                        <h2 class="title">Editar</h2>
                        <p class="subtitle">Editar registros.</p>
                        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                            <input type="hidden" name="idEditar" value="<?php echo $id;?>">
                            <div class="field is-horizontal">
                                <div class="field-label is-normal">
                                    <label class="label">Nome</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" name="nomeEditar" type="text" placeholder="Seu nome" value="<?php if (isset($data->nome)) { echo $data->nome; } ?>">
                                        </p>
                                    </div>
                                </div>
                                <div class="field-label is-normal">
                                    <label class="label">E-mail</label>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="input" name="emailEditar" type="text" placeholder="Seu e-mail" value="<?php if (isset($data->email)) { echo $data->email; }?>">
                                        </p>
                                    </div>
                                </div>
                                <div class="field-body">
                                    <div class="field">
                                        <p class="control">
                                            <input class="button" name="inserir" type="submit" value="Editar">
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="tile is-child notification is-info">
                        <!-- LISTA -->
                        <h2 class="title">Lista</h2>
                        <p class="subtitle">Listar registros.</p>
                        <div style="overflow-x: auto;">
                            <table class="table is-fullwidth">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                                        <?php foreach($listagem as $linha) { ?>
                                            <tr>
                                            <td><?php echo $linha->id;?></td>
                                            <td><?php echo $linha->nome;?></td>
                                            <td><?php echo $linha->email;?></td>
                                            <td>
                                                <button class="button is-small is-warning is-light" name="idEdita" type="submit" value="<?php if (isset($linha->id)) { echo $linha->id; }?>"><i class="fas fa-edit"></i></button>
                                                <button class="button is-small is-danger is-light" name="idApaga" type="submit" value="<?php if (isset($linha->id)) { echo $linha->id; }?>" onclick="return confirm('Tem certeza?')"><i class="fas fa-trash"></i></button>
                                            </td>
                                            </tr>
                                        <?php } ?>
                                    </form>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="tile is-child notification is-danger">
                        <h2 class="title">Outros Exemplos</h2>
                        <p class="subtitle"></p>
                        <a href="pag.php" class="button">Paginação</a>
                        <a href="ajax.php" class="button">AJAX</a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <footer class="footer">
        <div class="content has-text-centered">
            <p>
                Feito com o <strong>Bulma</strong> por <a href="https://lucasbrum.net">Lucas Saliés Brum</a>. Código fonte no <a href="https://gitlab.com/sistematico/spa-crud">Gitlab</a>.
            </p>
        </div>
    </footer>
<script>
document.addEventListener('DOMContentLoaded', () => {
    (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
        $notification = $delete.parentNode;
        $delete.addEventListener('click', () => {
            $notification.parentNode.removeChild($notification);
        });
    });
});
</script>
</body>
</html>