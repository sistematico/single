# Single Page CRUD

Projeto de CRUD(Create, Read, Update & Delete) em uma única página(Single Page Application(Não confundir com SPA!)).

## Demo

- [Single Page](https://single.lucasbrum.net)

## Ajude

Doe qualquer valor através do <a href="https://pag.ae/bfxkQW"><img src="https://img.shields.io/badge/doe-pagseguro-green"></a> ou <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DWHJL387XNW96&source=url"><img src="https://img.shields.io/badge/doe-paypal-blue"></a>

## ScreenShot

![Screenshot][screenshot]

[screenshot]: https://gitlab.com/sistematico/single/raw/master/single.png "ScreenShot"